# frozen_string_literal: true

class Followship < ApplicationRecord
  belongs_to :follower, class_name: 'User'
  belongs_to :followed, class_name: 'User'
  validate :follow_user_rule, on: :create
  validates :followed_id, uniqueness: { scope: %i[follower_id] }

  def follow_user_rule
    if follower_id == followed_id
      errors.add(
        :followships,
        'Followed and follower must be different users!'
      )
    end
  end
end
