# frozen_string_literal: true

class User < ApplicationRecord
  has_many :followed_users, foreign_key: :follower_id, class_name: 'Followship', dependent: :destroy
  has_many :followed, through: :followed_users
  has_many :following_users, foreign_key: :followed_id, class_name: 'Followship', dependent: :destroy
  has_many :followers, through: :following_users
  has_many :posts, dependent: :destroy
  has_many :likes, dependent: :destroy
  has_many :liked_posts, through: :likes, source: :post
  has_many :comments, dependent: :destroy
  devise :database_authenticatable, :registerable, :rememberable, :validatable
  validates :first_name, length: { minimum: 1, maximum: 100, allow_blank: false }
  validates :last_name, length: { minimum: 1, maximum: 100, allow_blank: false }
  validates :email, uniqueness: true, length: { minimum: 6, maximum: 100 }

  def full_name
    "#{first_name} #{last_name}"
  end
end
