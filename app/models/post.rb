# frozen_string_literal: true

class Post < ApplicationRecord
  include ImageUploader::Attachment(:image)
  belongs_to :user
  validates :text, length: { maximum: 100 }
  validates :title, length: { minimum: 1, maximum: 50, allow_blank: false }
  has_many :likes, dependent: :destroy
  has_many :comments, dependent: :destroy
end
