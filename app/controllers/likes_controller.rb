# frozen_string_literal: true

class LikesController < ApplicationController
  before_action :authenticate_user!
  def create
    post = Post.find(params[:id])
    current_user.liked_posts << post
    redirect_back(fallback_location: user_path(post.user))
  rescue ActiveRecord::RecordInvalid
    flash[:danger] = 'Validation failed: User has already been taken'
    redirect_back(fallback_location: user_path(post.user))
  end

  def destroy
    like = Like.find(params[:id])
    user = like.post.user
    like.destroy
    redirect_back(fallback_location: user_path(user))
  end
end
