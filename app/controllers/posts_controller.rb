# frozen_string_literal: true

class PostsController < ApplicationController
  before_action :authenticate_user!
  def index
    @posts = Post.all
  end

  def new
    @post = Post.new
  end

  def create
    @post = Post.new(post_params.merge(user: current_user))
    if @post.save
      flash[:success] = 'Post created successfully!'
      redirect_to user_path(current_user)
    else
      render 'new'
    end
  rescue ActiveRecord::NotNullViolation
    flash[:danger] = 'All rows must be filled!'
    redirect_back fallback_location: root_path
  end

  def show
    @post = Post.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:danger] = 'Order is missing!'
    redirect_to root_path
  end

  def destroy
    @post = Post.find(params[:id])
    if @post.destroy
      flash[:success] = 'Post deleted successfully!'
      redirect_to user_path(current_user)
    end
  end

  private

  def post_params
    params.require(:post).permit(:title, :text, :image, :user_id)
  end
end
