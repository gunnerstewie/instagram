# frozen_string_literal: true

class CommentsController < ApplicationController
  before_action :authenticate_user!
  def create
    @comment = Comment.new(comment_params.merge(user: current_user))
    if @comment.save
      flash[:success] = 'Comment added to post'
      redirect_back(fallback_location: user_path(@comment.post.user))
    else
      flash[:danger] = @comment.errors.full_messages
      redirect_to user_path(@comment.post.user)
    end
  end

  def destroy
    @comment = Comment.find(params[:id])
    if @comment.destroy
      flash[:success] = 'comment deleted!'
    else
      flash[:danger] = @comment.errors.full_messages
    end
    redirect_to user_path(@comment.post.user)
  end

  private

  def comment_params
    params.require(:comment).permit(:user_id, :post_id, :text)
  end
end
