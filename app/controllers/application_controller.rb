# frozen_string_literal: true

class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  helper_method :current_user_feed?, :followed_user_feed?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: %i[first_name last_name password_confirmation])
  end

  def current_user_feed?
    @user == current_user
  end

  def followed_user_feed?
    current_user.followed.include?(@user)
  end
end
