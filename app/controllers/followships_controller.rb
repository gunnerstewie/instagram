# frozen_string_literal: true

class FollowshipsController < ApplicationController
  before_action :authenticate_user!
  def create
    user = User.find(params[:id])
    current_user.followed << user
    redirect_back(fallback_location: user_path(user))
  rescue ActiveRecord::RecordInvalid => e
    flash[:error] = e.record.errors.full_messages.to_sentence
    redirect_back(fallback_location: user_path(user))
  end

  def destroy
    followship = Followship.find(params[:id])
    user = followship.followed
    followship.destroy
    redirect_back(fallback_location: user_path(user))
  end
end
