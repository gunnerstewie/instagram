# frozen_string_literal: true

class PagesController < ApplicationController
  before_action :authenticate_user!
  def index
    @users = User.all.excluding(current_user)
  end
end
