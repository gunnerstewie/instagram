# Instagram by Leskov Aleksey



* Ruby version 3.0.2

* Rails version 6.1.4

* For loading pictures used shrine

* As database used postgres

* For creating database run in terminal(on your app directory) `rails db:setup`

* To run the test suite `rspec`

* To ispect application with rubocop run `rubocop`, to fix: `rubocop -a` or `-A`

* You can run this application in Docker.
    - For this you must install Docker on your mashine.
    - Then after cloning application u must run terminal on directory of app and run commands: 
    - `docker-compose build`, `docker-compose up`

* If you want deploy app on heroku, go to:
    - https://devcenter.heroku.com/articles/getting-started-with-rails6
    - and follow instructions

# Main features: 
    
## User(by Devise)
- you can sign up and sign in to app

## Post
- as authenticated user you can create post:
- with: title, text message
- also you can load picture to your post

## Follow
- as authenticated user you can follow/unfollow another user
- this action needed to see feed of followed user

## Like
- as authenticated user you can like/unlike your own posts
- as follower you can like/unlike posts of followed users

## Comment
- as authenticated user you can comment/delete comments your own posts
- as follower you can comment/delete(your) comments of followed users


