# frozen_string_literal: true

class AddLikeTable < ActiveRecord::Migration[6.1]
  def change
    create_table :likes do |t|
      t.references :user, null: false, foreign_key: true, index: true
      t.references :post, null: false, foreign_key: true, index: true

      t.timestamps
    end
    add_index :likes, %i[user_id post_id], unique: true
  end
end
