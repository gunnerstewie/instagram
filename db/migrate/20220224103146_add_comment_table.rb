# frozen_string_literal: true

class AddCommentTable < ActiveRecord::Migration[6.1]
  def change
    create_table :comments do |t|
      t.references :user, null: false, foreign_key: true, index: true
      t.references :post, null: false, foreign_key: true, index: true
      t.string :text, null: false, limit: 100

      t.timestamps
    end
  end
end
