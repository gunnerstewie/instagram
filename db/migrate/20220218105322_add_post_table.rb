# frozen_string_literal: true

class AddPostTable < ActiveRecord::Migration[6.1]
  def change
    create_table :posts do |t|
      t.string :title, null: false, limit: 50
      t.string :text, limit: 100
      t.references :user, null: false, foreign_key: true, index: true

      t.timestamps
    end
  end
end
