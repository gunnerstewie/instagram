# frozen_string_literal: true

FactoryBot.define do
  factory :post do
    title { 'Title' }
    text { 'Text' }
    user
  end
end
