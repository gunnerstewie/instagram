# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'POST /followships', type: :request do
  let(:path) { '/followships' }
  let!(:user) { create(:user) }
  let!(:second_user) { create(:user) }
  let(:size) { Followship.count }

  context 'when valid params given' do
    it 'current_user follows another user' do
      sign_in user
      post path, params: { id: second_user.id }
      followship = Followship.find_by(followed_id: second_user)

      expect(response).to have_http_status :found
      expect(response).to redirect_to(user_path(second_user))

      expect(followship.followed_id).to eq(second_user.id)
      expect(size).to eq(1)
    end
  end

  context 'when user try to follow followed user' do
    it 'dont create follow and rise exception' do
      sign_in user
      post path, params: { id: second_user.id }
      post path, params: { id: second_user.id }

      expect(response).to redirect_to(user_path(second_user))
      expect(flash[:error]).to match('Followed has already been taken')
      expect(size).to eq(1)
    end
  end

  context 'when invalid params given' do
    it 'dont create follow and rise exception' do
      sign_in second_user
      post path, params: { id: second_user.id }

      expect(response).to redirect_to(user_path(second_user))
      expect(flash[:error]).to match('Followships Followed and follower must be different users!')
      expect(size).to eq(0)
    end
  end
end
