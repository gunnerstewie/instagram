# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'DELETE /followships/:id', type: :request do
  let(:unfollow_path) { "/followships/#{followship.id}" }
  let!(:followship) { Followship.create(followed: user, follower: second_user) }
  let!(:user) { create(:user) }
  let!(:second_user) { create(:user) }
  let(:size) { Followship.count }

  context 'when user is followed by current user' do
    it 'current user unfollows another user' do
      sign_in second_user
      delete unfollow_path
      expect(response).to have_http_status :found
      expect(response).to redirect_to(user_path(user))
      expect(size).to eq(0)
    end
  end
end
