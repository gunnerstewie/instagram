# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'DELETE /posts/:id', type: :request do
  let(:user) { create(:user) }
  let(:path) { "/posts/#{post.id}" }
  let(:post) { create(:post) }
  let(:size) { Post.count }

  context 'when user post' do
    it 'delete post successfully' do
      sign_in user
      delete path

      expect(size).to eq(0)
      expect(response).to redirect_to(user_path(user))
    end
  end

  context 'when user not logged in' do
    it 'redirect to sign_in page' do
      delete path

      expect(response).to have_http_status :found
      expect(response).to redirect_to(new_user_session_path)
    end
  end
end
