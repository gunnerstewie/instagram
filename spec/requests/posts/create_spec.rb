# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'POST /posts', type: :request do
  let(:path) { '/posts' }
  let(:user) { create(:user) }
  let(:params) { { post: { title: 'title', text: 'text', user_id: user } } }
  let(:size) { Post.count }

  context 'when valid params given' do
    it 'create a new post' do
      sign_in user
      post path, params: params
      post = Post.find_by(user_id: user)

      expect(response).to have_http_status :found
      expect(response).to redirect_to(user_path(user))

      expect(post.title).to eq('title')
      expect(size).to eq(1)
    end
  end

  context 'when user not logged in' do
    it 'redirect to sign_in page' do
      post path, params: params

      expect(response).to have_http_status :found
      expect(response).to redirect_to(new_user_session_path)
    end
  end

  context 'when wrong params given' do
    let(:params) { { post: { title: 'title' * 50, text: 'text', user_id: user } } }

    it 'render a new post page again' do
      sign_in user
      post path, params: params

      expect(response).to have_http_status :ok
      expect(response).to render_template('new')
    end
  end
end
