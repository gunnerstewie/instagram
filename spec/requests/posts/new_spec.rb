# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'GET /posts/new', type: :request do
  let(:path) { '/posts/new' }
  let(:user) { create(:user) }

  context 'when open new post page' do
    it 'successfully open page and render template' do
      sign_in user
      get path

      expect(response).to have_http_status :success
      expect(response).to render_template('new')
    end
  end

  context 'when user not logged in' do
    it 'redirect to sign_in page' do
      get path

      expect(response).to have_http_status :found
      expect(response).to redirect_to(new_user_session_path)
    end
  end
end
