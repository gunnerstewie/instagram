# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'POST /users', type: :request do
  let(:path) { '/users' }

  let(:params) do
    { user: { first_name: 'Alexey', last_name: 'Leskov', email: 'exampole@gmail.com', password: '123456' } }
  end
  let(:size) { User.count }

  context 'when valid params given' do
    it 'creates new user' do
      post path, params: params
      user = User.find_by(email: 'exampole@gmail.com')

      expect(response).to have_http_status :found
      expect(response).to redirect_to(root_path)

      expect(user.first_name).to eq('Alexey')
      expect(flash[:notice]).to match('Welcome! You have signed up successfully.')
      expect(size).to eq(1)
    end
  end

  context 'when wrong params given' do
    let(:params) { { user: { first_name: 'Alexey' } } }

    it 'redirect to new page' do
      post path, params: params

      expect(response).to have_http_status :success
      expect(response).to render_template('new')
    end
  end
end
