# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'GET /users/:id', type: :request do
  let(:path) { "/users/#{user.id}" }
  let(:user) { create(:user) }
  let(:new_post_path) { '/posts' }
  let(:post_params) { { post: { title: 'title', text: 'text', user_id: user } } }

  context 'when current_user open his empty show page' do
    it 'successfully open page and render template' do
      sign_in user
      get path

      expect(response).to have_http_status :success
      expect(response).to render_template('show')
      expect(response.body).to include('Your feed')
      expect(response.body).to include(new_post_path)
    end
  end

  context 'when current_user open his show page with created post' do
    it 'open page and show user,s feed' do
      sign_in user
      post new_post_path, params: post_params
      get path
      users_post = Post.find_by(user_id: user)

      expect(response).to have_http_status :success
      expect(response).to render_template('show')

      expect(response.body).to include('Your feed')
      expect(response.body).to include(new_post_path)
      expect(response.body).to include(likes_path)
      expect(response.body).to include('your comment')
      expect(response.body).to include('There is no comments yet.')

      expect(users_post.title).to eq(post_params[:post][:title])
    end
  end

  context 'when wrong user given' do
    let(:path) { '/users/666' }

    it 'redirect to root path with flash' do
      sign_in user
      get path

      expect(flash[:danger]).to match('User not found!')
      expect(response).to redirect_to(root_path)
    end
  end
end
