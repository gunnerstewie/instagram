# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'GET /users/sign_up', type: :request do
  let(:path) { '/users/sign_up' }

  context 'when open new user page' do
    it 'successfully open page and render template' do
      get path

      expect(response).to have_http_status :success
      expect(response).to render_template('new')
    end
  end
end
