# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'DELETE /likes/:id', type: :request do
  let(:unlike_path) { "/likes/#{like.id}" }
  let!(:like) { Like.create(post: posted, user: user) }
  let!(:user) { create(:user) }
  let(:posted) { create(:post) }
  let(:size) { Like.count }

  context 'when user unlike liked post' do
    it 'user unlike post successfully' do
      sign_in user
      delete unlike_path

      expect(response).to have_http_status :found
      expect(response).to redirect_to(user_path(posted.user))
      expect(size).to eq(0)
    end
  end

  context 'when user not logged in' do
    it 'redirect to sign_in page' do
      delete unlike_path

      expect(response).to have_http_status :found
      expect(response).to redirect_to(new_user_session_path)
    end
  end
end
