# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'POST /likes', type: :request do
  let(:path) { '/likes' }

  let(:user) { create(:user) }
  let(:posted) { create(:post) }
  let(:size) { Like.count }

  context 'when logged in user like a post' do
    it 'like post successfully' do
      sign_in user
      post path, params: { id: posted.id }
      like = Like.find_by(post_id: posted)

      expect(response).to have_http_status :found
      expect(response).to redirect_to(user_path(posted.user))

      expect(like.user).to eq(user)
      expect(size).to eq(1)
    end
  end

  context 'when user try to like liked post' do
    it 'dont create one more like and rise exception' do
      sign_in user
      post path, params: { id: posted.id }
      post path, params: { id: posted.id }

      expect(response).to redirect_to(user_path(posted.user))
      expect(flash[:danger]).to match('Validation failed: User has already been taken')
      expect(size).to eq(1)
    end
  end

  context 'when user not logged in' do
    it 'redirect to sign_in page' do
      post path, params: { post_id: posted.id }

      expect(response).to have_http_status :found
      expect(response).to redirect_to(new_user_session_path)
    end
  end
end
