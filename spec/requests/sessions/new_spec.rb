# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'GET /users/sign_in', type: :request do
  let(:path) { '/users/sign_in' }

  context 'when go to user sign_in path' do
    it 'successfully do request' do
      get path

      expect(response).to have_http_status :success
      expect(response).to render_template('new')
    end
  end

  context 'when user already signed in' do
    let(:user) { create(:user) }

    it 'redirect to root path' do
      sign_in user
      get path

      expect(response).to have_http_status :found
      expect(response).to redirect_to(root_path)
      expect(flash[:alert]).to match('You are already signed in.')
    end
  end
end
