# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'POST /users/sign_out', type: :request do
  let(:user) { create(:user) }

  context 'when destroy user session' do
    it 'log out user' do
      sign_out user
      get root_path

      expect(response).not_to render_template(:index)
    end
  end
end
