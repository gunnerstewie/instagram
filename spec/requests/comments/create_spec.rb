# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'POST /comments', type: :request do
  let(:path) { '/comments' }

  let!(:user) { create(:user) }
  let(:posted) { create(:post, user_id: user.id) }
  let(:params) { { comment: { post_id: posted.id, text: 'text', user_id: user.id } } }
  let(:size) { Comment.count }

  context 'when valid params given' do
    it 'create a new comment on post' do
      sign_in user
      post path, params: params
      comment = Comment.find_by(post_id: posted, user_id: user)
      expect(response).to have_http_status :found
      expect(response).to redirect_to(user_path(user))
      expect(comment.text).to eq('text')
      expect(size).to eq(1)
    end
  end

  context 'when user not logged in' do
    it 'redirect to sign_in page' do
      post path, params: params
      expect(response).to have_http_status :found
      expect(response).to redirect_to(new_user_session_path)
    end
  end

  context 'when empty text given' do
    let(:params) { { comment: { text: '', post_id: posted.id, user_id: user.id } } }

    it 'flash error and redirect_to users path' do
      sign_in user
      post path, params: params
      expect(response).to have_http_status :found
      expect(response).to redirect_to(user_path(user))
      expect(flash[:danger]).to match(['Text is too short (minimum is 1 character)'])
    end
  end

  context 'when too long text given' do
    let(:params) { { comment: { text: 'a' * 101, post_id: posted.id, user_id: user.id } } }

    it 'flash error and redirect_to users path' do
      sign_in user
      post path, params: params
      expect(response).to have_http_status :found
      expect(response).to redirect_to(user_path(user))
      expect(flash[:danger]).to match(['Text is too long (maximum is 100 characters)'])
    end
  end
end
