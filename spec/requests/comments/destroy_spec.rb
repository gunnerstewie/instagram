# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'DELETE /comments/:id', type: :request do
  let(:path) { "/comments/#{comment.id}" }
  let(:comment) { Comment.create(post: posted, user: user, text: 'new comment') }
  let(:user) { create(:user) }
  let(:posted) { create(:post, user: user) }
  let(:size) { Comment.count }
  let(:second_user) { create(:user) }

  context 'when user delete his comment on his post' do
    it 'user delete comment successfully' do
      sign_in user
      delete path

      expect(response).to have_http_status :found
      expect(response).to redirect_to(user_path(posted.user))
      expect(size).to eq(0)
    end
  end

  context 'when user delete any comment on his post' do
    let(:comment) { Comment.create(post: posted, user: second_user, text: 'another comment') }

    it 'delete any comment successfully' do
      sign_in user
      delete path

      expect(response).to have_http_status :found
      expect(response).to redirect_to(user_path(posted.user))
      expect(size).to eq(0)
    end
  end

  context 'when user delete his own comments on any post' do
    let(:posted) { create(:post) }

    it 'user delete comment successfully' do
      sign_in user
      delete path

      expect(response).to have_http_status :found
      expect(response).to redirect_to(user_path(posted.user))
      expect(size).to eq(0)
    end
  end

  context 'when user not logged in' do
    it 'redirect to sign_in page' do
      delete path

      expect(response).to have_http_status :found
      expect(response).to redirect_to(new_user_session_path)
    end
  end
end
