# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'pages/index', type: :view do
  context 'with list of users' do
    let!(:user) { create(:user) }
    let!(:second) { create(:user) }

    it 'displays list of users' do
      @users = User.all
      sign_in user
      render

      expect(rendered).to match(second.email)
      expect(rendered).to match(/Welcome to Instagram!/)
      expect(rendered).to match(/List of users/)
    end
  end

  context 'when check controller path' do
    it 'infers the controller path' do
      expect(controller.request.path_parameters[:controller]).to eq('pages')
      expect(controller.controller_path).to eq('pages')
    end
  end
end
