# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'devise/sessions/new.html.haml', type: :view do
  before do
    assign(:user, User.new(first_name: 'Alexey', last_name: 'Leskov', email: 'example@gmail.com'))
  end

  context 'when render text' do
    it 'render new user form' do
      render

      expect(rendered).to match(/Email/)
      expect(rendered).to match(/Password/)
      expect(rendered).to match(/Remember me/)
      expect(rendered).to match(/Sign up/)
      expect(rendered).to match(/Log in/)
    end
  end

  context 'when check controller action' do
    it 'infers the controller action' do
      expect(controller.request.path_parameters[:action]).to eq('new')
    end
  end
end
