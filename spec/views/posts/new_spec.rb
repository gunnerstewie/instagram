# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'posts/new.html.haml', type: :view do
  let(:user) { create(:user) }

  before do
    assign(:post, Post.new(title: 'Title', text: 'Text', user_id: user))
  end

  context 'when render text' do
    it 'render new user form' do
      sign_in user
      render

      expect(rendered).to match(/Title/)
      expect(rendered).to match(/Text/)
      expect(rendered).to match(/Create new post/)
      expect(rendered).to match("Greetings! #{user.email} Let's create a new post!")
      expect(rendered).to match(/Image/)
    end
  end

  context 'when check controller action' do
    it 'infers the controller action' do
      expect(controller.request.path_parameters[:action]).to eq('new')
    end
  end
end
