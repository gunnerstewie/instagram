# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  it { is_expected.to allow_value('index@gmail.com').for(:email) }
  it { is_expected.not_to allow_value('wrong').for(:email) }
  it { is_expected.to validate_length_of(:first_name).is_at_least(1).is_at_most(100) }
  it { is_expected.to have_many(:followed_users) }
  it { is_expected.to have_many(:followed).through(:followed_users) }
  it { is_expected.to have_many(:following_users) }
  it { is_expected.to have_many(:followers).through(:following_users) }
  it { is_expected.to have_many(:posts) }
  it { is_expected.to have_many(:likes) }
  it { is_expected.to have_many(:liked_posts).through(:likes) }
end
