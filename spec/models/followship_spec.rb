# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Followship, type: :model do
  it { is_expected.to belong_to(:follower) }
  it { is_expected.to belong_to(:followed) }
  it { is_expected.to validate_uniqueness_of(:followed_id).scoped_to(:follower_id) }
end
