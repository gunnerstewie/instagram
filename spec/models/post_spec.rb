# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Post, type: :model do
  it { is_expected.to validate_length_of(:title).is_at_least(1).is_at_most(50) }
  it { is_expected.to validate_length_of(:text).is_at_most(100) }
  it { is_expected.to belong_to(:user) }
  it { is_expected.to have_many(:likes) }
end
