# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Like, type: :model do
  subject { described_class.new(user_id: user.id, post_id: post.id) }

  let!(:user) { create(:user) }
  let!(:post) { create(:post) }

  it { is_expected.to belong_to(:user) }
  it { is_expected.to belong_to(:post) }
  it { is_expected.to validate_uniqueness_of(:user_id).scoped_to(:post_id) }
end
