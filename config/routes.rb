# frozen_string_literal: true

Rails.application.routes.draw do
  root 'pages#index'
  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :users, only: %i[index show]
  resources :posts
  resources :comments
  resources :followships, only: %i[create destroy]
  resources :likes, only: %i[create destroy]
end
